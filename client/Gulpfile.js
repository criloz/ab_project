'use strict';
var gulp = require('gulp');
var ts = require('gulp-typescript');
var merge = require('merge2');
var babel = require('gulp-babel');
var include  = require("gulp-include");
var insert = require('gulp-insert');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var stylus = require('gulp-stylus');
var nib = require('nib');
var gutil = require('gulp-util');
var shell = require('gulp-shell');
var tap = require('gulp-tap');
var glob = require("glob")
var fs = require('fs');



/*
 *  ======================================================================
 *   bundle  third party libraries https://github.com/wiledal/gulp-include
 *  ======================================================================
*/



gulp.task('alcony:build', ['alcony:webpack']);


gulp.task('story_book', shell.task(['node_modules/.bin/start-storybook -p 9001']));


/*
 ==========================================================================
    Karma tasks
 =========================================================================
 */
gulp.task('karma:run', shell.task(['node_modules/.bin/karma start']));


/*
==========================================================================
 Story book https://github.com/kadirahq/react-storybook
 =========================================================================
 */

gulp.task('storybook:run', shell.task(['node_modules/.bin/start-storybook -p 1965']));

gulp.task('storybook:create:config', function(callback){

    glob("src/**/*.story.+(ts|tsx)", function (er, files) {


        if (er) return callback(er);
        var requires = "";
        files.forEach(function(element, index, array){
            element = element.replace('.tsx','').replace('.ts','');
            requires += `require('../${element}');\n`;
        });

        var config_template = `import { configure } from '@kadira/storybook';
function loadStories() {
    // require as many stories as you need.
    ${requires}
}
configure(loadStories, module);`;

        fs.writeFile(".storybook/config.js", config_template, function(err) {
            if(err) {
                return callback(err);
            }

            console.log("The file was saved!");
            callback()
        });
        // files is an array of filenames.
        // If the `nonull` option is set, and nothing
        // was found, then files is ["**/*.js"]
        // er is an error object or null.
    })




});