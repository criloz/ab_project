const webpack = require('webpack');
const fs = require('fs');
const path = require('path'),
    join = path.join,
    resolve = path.resolve;
const configure = require("./.storybook/webpack.config.js")


const root = resolve(__dirname);
const codePath = join(root, 'src');
const appFile = join(codePath, 'client.tsx');
const modules = join(root, 'node_modules');
const dest = join(root, 'dist');
const tsConfigFile = join(root, 'client.dev.tsconfig.json');


changes = {
    entry: ["babel-polyfill",
        appFile
    ],
    output: {
        filename: "app.js",
        path: dest
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "cheap-source-map"
};


newConf = Object.assign({}, configure, changes);
module.exports = newConf;


