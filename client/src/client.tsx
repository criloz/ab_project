/// <reference path='all.d.ts' />

import * as React from "react";
import * as ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {Link, Router, Route, browserHistory, IndexRedirect} from "react-router";
import {syncHistoryWithStore} from "react-router-redux";
import {client, store} from "./models[redux|apollo]/store";
import {setScroll, setViewPortSize} from "./models[redux|apollo]/browser/browser.actions"
import {DefaultLayout} from "./components/layouts/default";
import {ABTestForm} from "./components/views/ab_test_form"
import {ABTestList} from "./components/views/ab_test_list"
import {Stats} from  "./components/views/stats"

import {connect} from "react-redux";
import {appStoreInterface} from "./models[redux|apollo]/app.actions";
import {BrowserImmutable} from "./models[redux|apollo]/browser/browser.actions";
import { ApolloProvider } from 'react-apollo';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();

const main_style = require("./stylus/main.styl");
// expose react for development 
window.React = React;
const rootEl = document.getElementById('root');


window.STORE = store;



// listen scroll
window.addEventListener("scroll", (event)=>{
    let width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    let height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    store.dispatch([setScroll(window.pageXOffset, window.pageYOffset), setViewPortSize(width, height)]);
});

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store);
/*
 class Client extends React.Component<any, any>{
 render() {
 return
 }
 };

 const ConnectedClient = connect(state => state)(Client);
 */
const App = (props)=> <MuiThemeProvider><DefaultLayout>{props.children}</DefaultLayout></MuiThemeProvider>;
const any = (props)=> <div style={{"height": "100%"}}>{props.children}</div>

ReactDOM.render(
    <ApolloProvider store={store} client={client}>
        <Router history={history}>
            <Route path="/app" component={App}>
                <IndexRedirect to="/app/abtests"/>
                <Route path="abtests" component={any}>
                    <IndexRedirect to="list"/>
                    <Route path="create" component={ABTestForm}/>
                    <Route path="edit/:id" component={ABTestForm}/>
                    <Route path="list" component={ABTestList}/>
                </Route>
                <Route path="stats/:id" component={Stats}/>
            </Route>
        </Router>
    </ApolloProvider>,
    rootEl
);

/*<IndexRedirect to="/scores"/>
 <Route path="connecting" component={ConnectingView}/>
 <Route path="login" component={LoginView} onEnter={RouterAccess.checkUserNotLogged}/>
 <Route path="is_logged" component={IsLogged} onEnter={RouterAccess.checkLogin}/>
 <Route path="scores" component={ScoresView} onEnter={RouterAccess.checkLogin}/>
 <Route path="agency/:id" component={AgencyView} onEnter={RouterAccess.checkLogin}/>*/