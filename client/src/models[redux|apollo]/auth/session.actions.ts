/// <reference path='../../all.d.ts' />

import {ACTIONS} from "../actions";
import * as Immutable from "immutable";
import {appStoreInterface} from "../app.actions";
import {addPendingRequest, removePendingRequest, networkFailedState} from "../backend/backend.actions";
import * as CONSTANTS from "../../constants";
import {browserHistory} from "react-router";


// ================================================================================
//                      Types for session store
// ================================================================================


type sessionProperties = "isLogged" | "loginErrors" | "isFetching" | "lastUpdated" | "requestAction";
type loginErrorsProperties = "badUserOrPassword";

export interface loginErrorsImmutable extends Immutable.Map<any, any> {
    get(key:loginErrorsProperties):any;
    get(key:"badUserOrPassword"):boolean;

    set(key:loginErrorsProperties, value:any):loginErrorsImmutable;
    set(key:"badUserOrPassword", value:boolean):loginErrorsImmutable;
}


export interface SessionImmutable extends Immutable.Map<any, any> {
    get(key:sessionProperties):any
    get(key:"isLogged"):boolean
    get(key:"isFetching"):boolean
    get(key:"lastUpdated"):number | void
    get(key:"requestAction"):ACTIONS | void
    get(key:"loginErrors"):loginErrorsImmutable

    set(key:sessionProperties, value:any):SessionImmutable
    set(key:"isLogged", value:boolean):SessionImmutable
    set(key:"isFetching", value:boolean):SessionImmutable
    set(key:"lastUpdated", value:number | void):SessionImmutable
    set(key:"requestAction", value:ACTIONS | void):SessionImmutable
    set(key:"loginErrors", value:loginErrorsImmutable):SessionImmutable

}

export interface SessionAction {
    type:ACTIONS
    requestAction?:ACTIONS
}
/*
 ================================================================================
                            Basic Actions
 ================================================================================
 */

export const loginUser = ():SessionAction => {
    return {
        type: ACTIONS.LOG_IN,
    }
};

export const logoutUser = ():SessionAction => {
    return {
        type: ACTIONS.LOG_OUT,
    }
};

export const badUserOrPasswordState = ():SessionAction => {
    return {
        type: ACTIONS.LOGIN_FAILED,
    }
};

export const startFetchLogin = ():SessionAction => {
    return {
        type: ACTIONS.SESSION_START_FETCH,
        requestAction: ACTIONS.LOG_IN
    }
};

export const startFetchLogout = ():SessionAction => {
    return {
        type: ACTIONS.SESSION_START_FETCH,
        requestAction: ACTIONS.LOG_OUT
    }
};


export const stopFetch = ():SessionAction => {
    return {
        type: ACTIONS.SESSION_STOP_FETCH,
    }
};
// ================================================================================
//                            Async Actions
// ================================================================================


/**
 *  login to  backend
 */
export const login = (username:string, password:string) => {
    const login_path = "login/";
    return function (dispatch, getState:{ ():appStoreInterface }) {
        const appState = getState().app;
        const backend = appState.get("backend");
        const session = appState.get("session");
        const sessionIsFetching = session.get("isFetching");
        // if the app is doing a login or logout request discard this action 
        if (sessionIsFetching) {
            return;
        }

        const csrfToken = backend.get("csrfToken");
        const backendUrl = backend.get("url");
        let data = new FormData();
        data.append("username", username);
        data.append("password", password);

        let headers = new Headers();
        headers.append("X-CSRFToken", <string>csrfToken);

        // tell to the app that a we are doing a login request
        dispatch([addPendingRequest(), startFetchLogin()]);
        // start login process
        return fetch(`${backendUrl}/${login_path}`, {
            method: 'POST',
            body: data,
            headers: headers,
            mode:'cors',
            credentials: 'include'
        }).then(
            login_info => {
                if (login_info.status == 200) {
                    login_info.text().then(
                        (data) => {
                            dispatch([removePendingRequest(), loginUser()]);
                            let state = getState();
                            let nextPath;
                            if (state.routing.locationBeforeTransitions.state) {
                                nextPath = state.routing.locationBeforeTransitions.state.nextPathname
                            }
                            else {
                                nextPath = CONSTANTS.DEFAULT_ROUTER_REDIRECT
                            }
                            browserHistory.push({pathname: nextPath, state: {nextPathname: ""}});
                            return
                        }
                    )
                }
                else {
                    return dispatch([removePendingRequest(), badUserOrPasswordState()])
                }
            },
            error => dispatch([removePendingRequest(), stopFetch(), networkFailedState()])
        )

    }
};


export const logout = () => {


}
