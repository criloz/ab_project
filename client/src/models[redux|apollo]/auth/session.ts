/// <reference path='../../all.d.ts' />

import * as Immutable from "immutable";
import {SessionImmutable, SessionAction} from "./session.actions";
import {ACTIONS} from "../actions";

/*
 ================================================================================
 Reducer
 ================================================================================
 */

export const sessionInitialState: SessionImmutable = Immutable.fromJS({
    isLogged: false,
    loginErrors: { badUserOrPassword: false },
    isFetching: false,
    requestAction: null,
    lastUpdated: null,
});


export function sessionReducer(state: SessionImmutable = sessionInitialState, action: SessionAction): SessionImmutable {
    switch (action.type) {
        case ACTIONS.LOG_IN:
            return state.withMutations((state: SessionImmutable) => {
                state
                    .set("isLogged", true)
                    .set("loginErrors", Immutable.fromJS({ badUserOrPassword: false }))
                    .set("lastUpdated", new Date().getTime())
                    .set("isFetching", false)
                    .set("requestAction", null);
            });
        case ACTIONS.LOG_OUT:
            return state.withMutations((state: SessionImmutable) => {
                state
                    .set("isLogged", false)
                    .set("loginErrors", Immutable.fromJS({ badUserOrPassword: false }))
                    .set("lastUpdated", new Date().getTime())
                    .set("isFetching", false)
                    .set("requestAction", null);
            });
        case ACTIONS.SESSION_START_FETCH:
            return state.withMutations((state: SessionImmutable) => {
                state.set("isFetching", true)
                    .set("requestAction", action.requestAction);
            });
        case ACTIONS.LOGIN_FAILED:
            return state.withMutations((state: SessionImmutable) => {
                state
                    .set("loginErrors", Immutable.fromJS({ badUserOrPassword: true }))
                    .set("lastUpdated", new Date().getTime())
                    .set("isFetching", false)
                    .set("requestAction", null);
            });
        case ACTIONS.SESSION_STOP_FETCH:
            return state.withMutations((state: SessionImmutable) => {
                state
                    .set("isFetching", false)
                    .set("requestAction", null);
            });

        default:
            return state
    }
};
