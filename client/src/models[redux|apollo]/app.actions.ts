
import { SessionImmutable, } from './auth/session.actions'
import { BackendImmutable, } from './backend/backend.actions'
import { BrowserImmutable, } from './browser/browser.actions'

//=================================================================================
//                                  Types for app store
// ================================================================================

export type AppProperties = "session" | "backend" | "browser"


export interface AppImmutable extends Immutable.Map<any, any> {
    get(key: AppProperties): any
    get(key: "session"): SessionImmutable
    get(key: "backend"): BackendImmutable
    get(key: "browser"): BrowserImmutable

    set(key: AppProperties, value: any): AppImmutable
    set(key: "session", value: SessionImmutable): AppImmutable
    set(key: "backend", value: BackendImmutable): AppImmutable
    set(key: "browser", value: BrowserImmutable): AppImmutable


}

export interface appStoreInterface {
    app?: AppImmutable
    routing?: {locationBeforeTransitions: HistoryModule.Location}
}
