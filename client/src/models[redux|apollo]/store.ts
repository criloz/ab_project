import {createStore, combineReducers, applyMiddleware, compose, Store} from "redux";
import {routerReducer, routerMiddleware} from "react-router-redux";
import {appReducer} from "./app";
import {appStoreInterface} from "./app.actions";
import thunk from "redux-thunk";
import multi from "redux-multi";
import {batchedSubscribe} from "redux-batched-subscribe";
import {browserHistory} from "react-router";
import ApolloClient, {createNetworkInterface} from "apollo-client";
let newRouterMiddleware = routerMiddleware(browserHistory);
const networkInterface = createNetworkInterface("http://127.0.0.1:8000/graphql")
//export const client = new ApolloClient({networkInterface, });
export const client = new ApolloClient();

// Add the reducer to your store on the `routing` key
export const store:Store<appStoreInterface> = createStore<appStoreInterface>(
    combineReducers(
        Object.assign({},
            {app: appReducer},
            {routing: routerReducer},
            {apollo: client.reducer()}
        )
    ), {}, compose(
        applyMiddleware(client.middleware()),
        applyMiddleware(newRouterMiddleware),
        applyMiddleware(thunk),
        applyMiddleware(multi),

        batchedSubscribe((notify) => {
            notify();
        })
        //,window.devToolsExtension && window.devToolsExtension()
    )
);

