/// <reference path='../../all.d.ts' />

import {ACTIONS} from "../actions";
import * as Immutable from "immutable";
import {logoutUser, loginUser} from "../auth/session.actions";
import {appStoreInterface} from "../app.actions";
import {browserHistory} from "react-router";
import * as CONSTANTS from "../../constants";

//=================================================================================
//                                  Types for backend store
// ================================================================================


type backendProperties = "url" | "csrfToken" | "isOnLine" | "networkError" | "pendingRequests" | "lastUpdated";


export interface BackendImmutable extends Immutable.Map<any, any> {
    get(key:backendProperties):any
    get(key:"url"):string | void
    get(key:"csrfToken"):string | void
    get(key:"isOnLine"):boolean
    get(key:"networkError"):boolean
    get(key:"pendingRequests"):number
    get(key:"lastUpdated"):number | void

    set(key:backendProperties, value:any):BackendImmutable
    set(key:"csrfToken", value:string | void):BackendImmutable
    set(key:"url", value:string):BackendImmutable
    set(key:"isOnLine", value:boolean):BackendImmutable
    set(key:"networkError", value:boolean):BackendImmutable
    set(key:"pendingRequests", value:number):BackendImmutable
    set(key:"lastUpdated", value:number):BackendImmutable

}

export interface backendAction {
    type:ACTIONS
    csrfToken?:string
    url?:string
}
export interface backendActionCreator {
    (...arg:any[]):backendAction
}

// ================================================================================
//                             Basic Actions
// ================================================================================


export const connectedState:backendActionCreator = function (url:string, csrfToken:string) {
    return {
        type: ACTIONS.CONNECT_TO_BACKEND,
        csrfToken: csrfToken,
        url: url
    }
}

export const addPendingRequest:backendActionCreator = () => {
    return {
        type: ACTIONS.ADD_PENDING_REQUEST
    }
}

export const removePendingRequest:backendActionCreator = () => {
    return {
        type: ACTIONS.REMOVE_PENDING_REQUEST
    }
}

export const networkFailedState:backendActionCreator = () => {
    return {
        type: ACTIONS.NETWORK_FAILED
    }

}

//================================================================================
//                            Async Actions
//================================================================================


/**
 * Check if the backend is on-line
 * retrieve important info like if the user is logged and the csrf token
 */
export const connectToBackend = (url:string) => {
    const connect_path = "connection_state/"
    return function (dispatch, getState:{ ():appStoreInterface }) {
        dispatch(addPendingRequest());
        return fetch(`${url}/${connect_path}`, {credentials:"include"}).then(
            connect_info => {
                if (connect_info.status == 200) {
                    connect_info.json<{ isLogged:boolean, csrfToken:string }>().then(
                        (data) => {
                            let multiDispatch:any[] = [removePendingRequest(), connectedState(url, data.csrfToken)]

                            if (data.isLogged) multiDispatch.push(loginUser())
                            else multiDispatch.push(logoutUser())

                            dispatch(multiDispatch)

                            let state = getState()
                            let nextPath

                            if (state.routing.locationBeforeTransitions.state) {
                                nextPath = state.routing.locationBeforeTransitions.state.nextPathname
                            }
                            else {
                                nextPath = CONSTANTS.DEFAULT_ROUTER_REDIRECT
                            }

                            browserHistory.push({pathname: nextPath, state: {nextPathname: ""}})
                            return
                        }
                    );

                }
                else {
                    return dispatch([removePendingRequest(), networkFailedState()])
                }
            },
            error => dispatch([removePendingRequest(), networkFailedState()])
        )

    }
}
