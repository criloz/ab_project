/// <reference path='../../all.d.ts' />

import * as Immutable from "immutable";
import {BackendImmutable, backendAction} from "./backend.actions";
import {ACTIONS} from "../actions";

/*
 ================================================================================
    Reducer
 ================================================================================
 */

export const backendInitialState: BackendImmutable = Immutable.fromJS({
    csrfToken: null,
    isOnLine: false,
    networkError: false,
    pendingRequests: 0,
    lastUpdated: null,
    url: null
});


export function backendReducer(state: BackendImmutable = backendInitialState, action: backendAction): BackendImmutable {
    switch (action.type) {
        case ACTIONS.CONNECT_TO_BACKEND:
            return state.withMutations((state: BackendImmutable) => {
                state.set("networkError", false)
                    .set("isOnLine", true)
                    .set("url", action.url)
                    .set("csrfToken", action.csrfToken)
                    .set("lastUpdated", new Date().getTime());
            });

        case ACTIONS.NETWORK_FAILED:
            return state.withMutations((state: BackendImmutable) => {
                state.set("networkError", true)
                    .set("lastUpdated", new Date().getTime());
            });
        case ACTIONS.ADD_PENDING_REQUEST:
            return state.set("pendingRequests", state.get("pendingRequests") + 1)
        case ACTIONS.REMOVE_PENDING_REQUEST:
            let pendingRequests = state.get("pendingRequests") - 1
            if (pendingRequests < 0) pendingRequests = 0;
            return state.set("pendingRequests", pendingRequests)
        default:
            return state
    }
};
