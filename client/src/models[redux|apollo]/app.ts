/**
 * Created by cristian on 10/06/16.
 */
import * as Immutable from "immutable";
import {ACTIONS} from "./actions";
import {sessionInitialState, sessionReducer} from "./auth/session";
import {backendInitialState, backendReducer} from "./backend/backend";
import {browserInitialState, browserReducer} from "./browser/browser";
import {AppImmutable} from "./app.actions";

// ================================================================================
//                              initialState
// ================================================================================

const AppInitialState:AppImmutable = Immutable.fromJS({
    session: sessionInitialState,
    backend: backendInitialState,
    browser: browserInitialState
});


// ================================================================================
//                             App reducer 
// =================================================================================


export function appReducer(state:AppImmutable = AppInitialState, action:{ type:ACTIONS }) {
    if (action.type >= ACTIONS.LOG_IN && action.type <= ACTIONS.LOGIN_FAILED) {
        return state.set("session", sessionReducer(state.get("session"), action))
    }
    else if (action.type >= ACTIONS.ADD_PENDING_REQUEST && action.type <= ACTIONS.CONNECT_TO_BACKEND) {
        return state.set("backend", backendReducer(state.get("backend"), action))
    }
    else if (action.type >= ACTIONS.SET_SCROLL_POSITION && action.type <= ACTIONS.SET_VIEWPORT_SIZE) {
        return state.set("browser", browserReducer(state.get("browser"), action))
    }
    else {
        return state
    }

}