/// <reference path='../../all.d.ts' />

import * as Immutable from "immutable";
import {BrowserImmutable, browserAction} from "./browser.actions";
import {ACTIONS} from "../actions";

/*
 ================================================================================
 Reducer
 ================================================================================
 */

export const browserInitialState:BrowserImmutable = Immutable.fromJS({
    scrollX: 0,
    scrollY: 0,
    width:0,
    height:0
});


export function browserReducer(state:BrowserImmutable = browserInitialState, action:browserAction):BrowserImmutable {
    switch (action.type) {
        case ACTIONS.SET_SCROLL_POSITION:
            return state.withMutations((state:BrowserImmutable) => {
                state.set("scrollX", action.scrollX)
                    .set("scrollY", action.scrollY)
            });
        case ACTIONS.SET_VIEWPORT_SIZE:
            return state.withMutations((state:BrowserImmutable) => {
                state.set("width", action.width)
                    .set("height", action.height)
            });
        default:
            return state
    }
};
