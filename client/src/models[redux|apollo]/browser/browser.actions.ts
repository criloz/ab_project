/// <reference path='../../all.d.ts' />

import {ACTIONS} from "../actions";
import * as Immutable from "immutable";

//=================================================================================
//                                  Types for backend store
// ================================================================================


type  browserProperties = "scrollX" | "scrollY" | "width" | "height";


export interface BrowserImmutable extends Immutable.Map<any, any> {
    get(key:browserProperties):any
    get(key:"scrollX"):number
    get(key:"scrollY"):number
    get(key:"width"):number
    get(key:"height"):number


    set(key:browserProperties, value:any):BrowserImmutable
    set(key:"scrollX", value:number):BrowserImmutable
    set(key:"scrollY", value:number):BrowserImmutable
    set(key:"width", value:number):BrowserImmutable
    set(key:"height", value:number):BrowserImmutable
}

export interface browserAction {
    type:ACTIONS
    scrollX?:number
    scrollY?:number
    width?:number
    height?:number
}
export interface browserActionCreator {
    (...arg:any[]):browserAction
}

// ================================================================================
//                             Basic Actions
// ================================================================================


export const setScroll:browserActionCreator = function (scrollX:number, scrollY:number):browserAction {
    return {
        type: ACTIONS.SET_SCROLL_POSITION,
        scrollX: scrollX,
        scrollY: scrollY
    }
};


export const setViewPortSize:browserActionCreator = function (width:number, height:number):browserAction {
    return {
        type: ACTIONS.SET_VIEWPORT_SIZE,
        width: width,
        height: height
    }
};