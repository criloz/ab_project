/**
 * Created by cristian on 10/06/16.
 */
export enum ACTIONS {
    // session
    LOG_IN=1,
    LOG_OUT,
    SESSION_START_FETCH,
    SESSION_STOP_FETCH,
    LOGIN_FAILED,
    
    // backend
    ADD_PENDING_REQUEST,
    REMOVE_PENDING_REQUEST,
    NETWORK_FAILED,
    CONNECT_TO_BACKEND,
    
    //Browser
    SET_SCROLL_POSITION,
    SET_VIEWPORT_SIZE
}