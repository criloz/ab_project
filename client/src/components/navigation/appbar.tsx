import * as React from "react"


const style = require("./appbar.styl");

export const AppBar = function (props) {
    return (
        <div className={style.app_bar}>
            <div>CreditSecret</div>
            <nav/>
        </div>
    )

};
