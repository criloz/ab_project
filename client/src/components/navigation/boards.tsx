import * as React from "react";

const style = require("./boards.styl");

export const Board = (props)=>{
    return <main className={style.board}>
        <section className={style.contents}>
            {props.children}
        </section>
    </main>
}

export const TwoSideNavsBoard = (props)=>{
    return <main id="board" className={style.tsn_board}>
        <section className={style.contents}>
            {props.children}
        </section>
        <section className={style.phantom_nav}/>
    </main>
}