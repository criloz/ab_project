/**
 * Created by cristian on 16/06/16.
 */
const style = require("./side_navs.styl");


export const LeftSideNav = (props)=>{
    return <section className={style.side_nav}></section>
}

export const RightSideNav = (props)=>{
    return <section id="right_side_nav" className={style.right_side_nav}></section>
}