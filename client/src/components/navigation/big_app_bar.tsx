import * as React from "react"
import {AppBar} from "./appbar"
const style = require("./big_app_bar.styl");
const classNames = require('classnames');


export interface BigAppBar{
    windowScrollX: number
    windowScrollY: number
    children?:any[]
}
export const BigAppBar = (props: BigAppBar)=> {
    let bar_shadow = {};
    bar_shadow[style.app_bar_section_shadow] =  props.windowScrollY>32;
    let bar_section_class = classNames(style.app_bar_section, bar_shadow);

    return (
        <header className={style.big_app_bar_no_fixed}>
            <section 
                className={bar_section_class}>
                <AppBar/>
            </section>
            <section className={style.header_content_section}>
                {props.children}
            </section>
        </header>
    )
};