import * as React from "react";
const style = require("./load_bars.styl");

export const QueryLoadBar=(props)=>{
    return <div>
        <div className={style.query_background}></div>
        <div className={style.query_progress}></div>
    </div>
};