/// <reference path='../../all.d.ts' />

import * as React from "react";
import {storiesOf} from "@kadira/storybook";
import {QueryLoadBar} from "./load_bars";

const story = storiesOf('Load Bars', module);

story.add("Query load bar", ()=>{return <QueryLoadBar/>});
