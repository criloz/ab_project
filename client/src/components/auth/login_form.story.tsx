/// <reference path='../../all.d.ts' />

import * as React from "react";
import {storiesOf} from "@kadira/storybook";
import {IsolatedLoginForm} from "./login_form";
import Cases from "./login_form.cases";

const story = storiesOf('Login Form', module);

let key:string;

for(key in Cases){

    function element(iKey){
        return function isolatedComponent(){
            return <IsolatedLoginForm {...Cases[iKey]}/>
        }
    }

    story.add(key, element(key))
}
