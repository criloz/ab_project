/**
 * Created by cristian on 5/06/16.
 */
import {loginFormInterFace} from "./login_form";
import {sessionInitialState} from "../../models[redux|apollo]/auth/session";
import * as Immutable from "immutable";

let Cases:{[id:string]:loginFormInterFace} = {};

function dispatch(action):any {
    console.log(action)
}

Cases["Default"] = {
    dispatch: dispatch,
    session: sessionInitialState
};

Cases["Bad User Or Password"] = {
    dispatch: dispatch,
    session: sessionInitialState.set("loginErrors", Immutable.fromJS({badUserOrPassword: true}))
};

Cases["Is Fetching"] = {
    dispatch: dispatch,
    session: sessionInitialState.set("isFetching", true)
};


export default Cases;
