/// <reference path='../../all.d.ts' />
import {login, SessionImmutable} from "../../models[redux|apollo]/auth/session.actions";
import {appStoreInterface} from "../../models[redux|apollo]/app.actions";
import * as React from "react";
import {connect} from "react-redux";
import {ClassDictionary} from "classnames";
const classNames = require('classnames');
const style = require("./login_form.styl");
export interface loginFormInterFace {
    session:SessionImmutable
    dispatch: {(action:any):any}
}


/*
 event handlers
 */
const handleInputEvent = (f:Function)=> {
    return function (e) {
        return f(e.target.value);
    }
};

const handleLoginEvent = (f:Function)=> {
    return function (e) {
        e.preventDefault();
        return f();
    }
};

const handleKeyPressEvent = (f:Function)=> {
    return function (e) {
        var p = e.which;
        if (p==13){
            e.preventDefault();
            return f();
        }
    }
};



/*
 Login form component
 */
function mapStateToProps(state:appStoreInterface, ownProps) {
    return {session: state.app.get("session")}
}

export class IsolatedLoginForm extends React.Component<loginFormInterFace, any> {
    private userName:string;
    private passWord:string;

    constructor(props?:loginFormInterFace,
                context?:any) {
        super(props, context);
        this.userName = "";
        this.passWord = "";
    }

    changePassword = (newPassword:string)=> {
        this.passWord = newPassword;
        this.forceUpdate()
    };

    changeUserName = (newUserName:string)=> {
        this.userName = newUserName;
        this.forceUpdate()
    };

    render() {
        let fireLogin = ()=>this.props.dispatch(login(this.userName, this.passWord))
        // email conditional classes
        let emailOptionalClasses:ClassDictionary = {};
        emailOptionalClasses[style.inputWithContents] = this.userName.trim() != "";
        const emailClass = classNames(style.input, emailOptionalClasses);

        // password conditional classes
        let passwordOptionalClasses:ClassDictionary = {};
        passwordOptionalClasses[style.inputWithContents] = this.passWord.trim() != "";
        const passWordClass = classNames(style.input, passwordOptionalClasses);
        return <form className={style.form}>
            <div className={style.group}>
                <input
                    id="loginFormEmail"
                    className={emailClass}
                    type="email"
                    onChange={handleInputEvent(this.changeUserName)}
                    required="true"
                    value={this.userName}
                    onKeyPress={handleKeyPressEvent(fireLogin)}

                />
                <label htmlFor="loginFormEmail" className={style.controlLabel}>Email address</label>
                <i className={style.bar}/>
            </div>
            <div className={style.group}>
                <input
                    id="loginFormPassword"
                    className={passWordClass}
                    type="password"
                    onChange={handleInputEvent(this.changePassword)}
                    required="true"
                    value={this.passWord}
                    onKeyPress={handleKeyPressEvent(fireLogin)}
                />
                <label htmlFor="loginFormPassword" className={style.controlLabel}>Password</label>
                <i className={style.bar}/>

            </div>
            {this.props.session.get("loginErrors").get("badUserOrPassword")?
             <span>Unknown user name or bad password</span>:
             ""
            }
            <a className={style.login_button}
               onClick={handleLoginEvent(fireLogin)}
               href="#">Log In</a>
        </form>;
    }
}

export const LoginForm = connect(mapStateToProps)(IsolatedLoginForm);
