/// <reference path='../../all.d.ts' />

import * as React from "react";
import {connect} from "react-apollo";
import AppBar from "material-ui/AppBar";
import {List, ListItem} from "material-ui/List";
import Assessment from "material-ui/svg-icons/action/assessment";
import Build from "material-ui/svg-icons/action/build";
import {abTestList} from "../views/ab_test_list";
const styles = require("./default.styl");
import {push} from "react-router-redux";

interface ABURL {
    value:string
}
interface ABTest {
    id:string
    name:string
    goalUrl:ABURL
    test1Url:ABURL
    test2Url:ABURL
    shortUrl:ABURL
    proportion:number
}

function mapQueriesToProps({ownProps, state}) {
    return {
        abTestList: abTestList,
    };
}
@connect({mapQueriesToProps,})
export class DefaultLayout extends React.Component<any,any> {
    render() {
        let edges:{node:ABTest}[] = [];
        if (!!this.props.abTestList) {
            if (!!this.props.abTestList.allAbTests) {
                edges = this.props.abTestList.allAbTests.edges;
            }
        }
        let nestedItems = [];
        for(let i in edges){
            nestedItems.push(
                <ListItem
                    key={edges[i].node.id}
                    onClick={(e)=>{this.props.dispatch(push("/app/stats/" + edges[i].node.id))}}
                    primaryText={edges[i].node.name} />)
        }
        return (
            <article className={styles.app}>
                <header><AppBar title="A/B Testing App"
                                iconClassNameRight="muidocs-icon-navigation-expand-more"/></header>
                <main className={styles.main}>
                    <menu className={styles.menu}>
                        <List>
                            <ListItem primaryText="A/B tests" leftIcon={<Build />} onClick={(e)=>{this.props.dispatch(push("/app/abtests/list"))}}/>
                            <ListItem
                                primaryText="Stats"
                                leftIcon={<Assessment />}
                                initiallyOpen={true}
                                primaryTogglesNestedList={true}
                                nestedItems={nestedItems}/>
                        </List>
                    </menu>
                    <section>
                        {this.props.children}
                    </section>
                </main>
            </article>
        )
    }

}
;