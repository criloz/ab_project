/// <reference path='../../all.d.ts' />

import * as React from "react";
import TextField from "material-ui/TextField";
import Slider from "material-ui/Slider";
import {Toolbar, ToolbarTitle} from "material-ui/Toolbar";
import RaisedButton from "material-ui/RaisedButton";
import {connect} from "react-apollo";
import gql from "graphql-tag";
import LinearProgress from "material-ui/LinearProgress";
import {push} from "react-router-redux";
import {abTestList} from "./ab_test_list";
import {client} from "../../models[redux|apollo]/store";
const styles = require("./ab_test_form.styl");
interface ABURL {
    value:string
}
interface ABTest {
    id:string
    name:string
    goalUrl:ABURL
    test1Url:ABURL
    test2Url:ABURL
    shortUrl:ABURL
    proportion:number
}

class ProportionSlider extends React.Component<{proportion:number, onChange:Function, disabled:boolean}, any> {
    handleSlider(event, value) {
        event = {target: {value: value}};
        this.props.onChange(event);
    }

    render() {
        return (
            <div className={styles.slider}>
                <span><b>Proportion:</b> {this.props.proportion * 100}%</span>
                <Slider value={this.props.proportion}
                        onChange={this.handleSlider.bind(this)}
                        disabled={this.props.disabled}
                />
            </div>
        );
    }
}
interface SaveMutation {
    (state:any):Promise<any>
}
interface ABTestFormInterface {
    id:string
    name:string
    loading:boolean
    apollo?:any
    dispatch?:any
    abTestList?:any
    errors:{
        name?:string
        goalUrl?:string
        test1Url?:string
        test2Url?:string
        proportion?:string
    }
    goalUrl:string
    test1Url:string
    test2Url:string
    proportion:number
    mutations?:{save:SaveMutation}
    save?:SaveMutation
    onCancel?:React.EventHandler<React.MouseEvent>
    onSave?:React.EventHandler<React.MouseEvent>
    onChange?:{(p:string):React.EventHandler<React.SyntheticEvent>}
    params?:any
}

export const ABTestFormIsolated = (props:ABTestFormInterface)=> {
    let visibility = props.loading ? "visible" : "hidden";
    return (
        <article className={styles.contents}>
            <form className={styles.form}>
                <Toolbar>
                    <ToolbarTitle text="Create A/B Test"/>
                </Toolbar>

                <LinearProgress mode="indeterminate" style={{visibility:visibility}}/>

                <fieldset>
                    <TextField
                        floatingLabelText="Name"
                        fullWidth={true}
                        onChange={props.onChange("name")}
                        errorText={props.errors.name}
                        disabled={props.loading}
                        value={props.name}
                    />
                    <TextField
                        floatingLabelText="Goal Url"
                        fullWidth={true}
                        onChange={props.onChange("goalUrl")}
                        errorText={props.errors.goalUrl}
                        disabled={props.loading}
                        value={props.goalUrl}
                    />
                    <TextField
                        floatingLabelText="Test Url 1"
                        fullWidth={true}
                        onChange={props.onChange("test1Url")}
                        errorText={props.errors.test1Url}
                        disabled={props.loading}
                        value={props.test1Url}
                    />
                    <TextField
                        floatingLabelText="Test Url 2"
                        fullWidth={true}
                        onChange={props.onChange("test2Url")}
                        errorText={props.errors.test2Url}
                        disabled={props.loading}
                        value={props.test2Url}

                    />
                    <ProportionSlider
                        proportion={props.proportion}
                        disabled={props.loading}
                        onChange={props.onChange("proportion")}/>
                </fieldset>

                <div className={styles.actions}>
                    <RaisedButton label="Cancel" secondary={true} onClick={props.onCancel}/>
                    <RaisedButton label="Save" primary={true} onClick={props.onSave}/>
                </div>
            </form>
        </article>
    )
};


function mapMutationsToProps({ownProps, state}) {
    return {
        save: (variables) => ({
            mutation: gql`
            mutation CreateAB($pk:ID, $name: String!, $proportion: Float!, $goalUrl: String!, $test1Url: String!, $test2Url: String!){
                createAbTest(pk:$pk, name:$name, proportion:$proportion, goalUrl: $goalUrl, test1Url: $test1Url, test2Url: $test2Url){
                abTest{
                    id,
                    name
                }
                ok
                errors
                }
            }`,
            variables: variables,
        }),
    };
}


function mapQueriesToProps({ownProps, state}) {
    return {
        abTestList: abTestList,
    };
}

function mapStateToProps(state, ownProps) {
    return {
        id: ownProps.params.id,
        filter: ownProps.location.query.filter,
        apollo: client
    };
}

@connect({mapQueriesToProps, mapMutationsToProps, mapStateToProps})
export class ABTestForm extends React.Component<ABTestFormInterface, any> {
    state:ABTestFormInterface = {
        id: null,
        name: "",
        goalUrl: "",
        test1Url: "",
        test2Url: "",
        proportion: 0.5,
        errors: {},
        loading: false
    };
    is_mounted = false;

    componentDidMount() {
        this.is_mounted = true
        if (!!this.props.id) {
            this.state.id = this.props.id;

            this.props.apollo.query({
                query: gql`
                query AbTest($id:ID!){
                  ABTest(id:$id){
                    id,
                    name,
                    goalUrl {value},
                    test1Url {value},
                    test2Url {value},
                    proportion,
                    shortUrl {value}
                    }
                 }`,
                forceFetch: true,
                variables: {
                    id: this.state.id
                },
            }).then((data)=> {
                let newState:any = {};
                let ABTest = data.data["ABTest"];
                newState.id = ABTest.id;
                newState.name = ABTest.name;
                newState.goalUrl = ABTest.goalUrl.value;
                newState.test1Url = ABTest.test1Url.value;
                newState.test2Url = ABTest.test2Url.value;
                if (ABTest.shortUrl) {
                    newState.shortUrl = ABTest.shortUrl.value;
                }
                else {
                    newState.shortUrl = "";
                }
                newState.proportion = ABTest.proportion;
                if (this.is_mounted) {
                    this.setState(newState)
                }

            });
        }
    }

    componentWillUnmount() {
        this.is_mounted = false
    }

    constructor(props?:any, context?:any) {
        super(props, context);

    }

    handleSave = (event)=> {
        this.setState({loading: true});
        this.props.mutations.save({
            // Use the container component's props
            pk: this.state.id,
            name: this.state.name,
            goalUrl: this.state.goalUrl,
            test1Url: this.state.test1Url,
            test2Url: this.state.test2Url,
            proportion: this.state.proportion,
        }).then((data)=> {
            this.setState({loading: false});
            let errors = JSON.parse(data.data["createAbTest"]["errors"]);
            this.setState({errors: errors});
            if (Object.keys(errors).length == 0) {
                this.props.abTestList.refetch();
                this.props.dispatch(push("/app/abtests/list"))
            }

        })
    };

    handleCancel = (event)=> {
        this.props.dispatch(push("/app/abtests/list"))
    };

    handleChange = (property:string)=> {
        return (event)=> {
            let newState = {};
            newState[property] = event.target.value;
            this.setState(newState)
        }
    };

    render() {
        return (
            <ABTestFormIsolated {...this.state}
                onCancel={this.handleCancel}
                onSave={this.handleSave}
                onChange={this.handleChange}/>
        )
    }

}



