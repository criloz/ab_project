/// <reference path='../../all.d.ts' />

import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from "material-ui/Table";
import * as React from "react";
import {Toolbar, ToolbarTitle, ToolbarGroup, ToolbarSeparator} from "material-ui/Toolbar";
import FontIcon from "material-ui/FontIcon";
import RaisedButton from "material-ui/RaisedButton";
import {push} from "react-router-redux";
import {connect} from "react-apollo";
import gql from "graphql-tag";
const styles = require("./ab_test_list.styl");


interface ABURL {
    value:string
}
interface ABTest {
    id:string
    name:string
    goalUrl:ABURL
    test1Url:ABURL
    test2Url:ABURL
    shortUrl:ABURL
    proportion:number
}

const ABTable = (props:{abTestList:{node:ABTest}[], onSelection:any, selectedRows:any})=> {
    return (<Table onRowSelection={props.onSelection}>
            <TableHeader>
                <TableRow>
                    <TableHeaderColumn>Name</TableHeaderColumn>
                    <TableHeaderColumn>Short URL</TableHeaderColumn>
                    <TableHeaderColumn>Goal Url</TableHeaderColumn>
                    <TableHeaderColumn>Test Url 1</TableHeaderColumn>
                    <TableHeaderColumn>Test Url 2</TableHeaderColumn>
                    <TableHeaderColumn>Proportion</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody>
                {props.abTestList.map(function (e, index) {
                    let selected = false;
                    if(props.selectedRows.length>0){
                        selected = props.selectedRows[0] == index
                    }

                    return (<TableRow key={e.node.id} selected={selected}>
                        <TableRowColumn>{e.node.name}</TableRowColumn>
                        {e.node.shortUrl ?
                            <TableRowColumn><a target="_blank" href={e.node.shortUrl.value}>{e.node.shortUrl.value}</a></TableRowColumn>:
                            <TableRowColumn></TableRowColumn>}
                        <TableRowColumn><a target="_blank"
                                           href={e.node.goalUrl.value}>{e.node.goalUrl.value}</a></TableRowColumn>
                        <TableRowColumn><a target="_blank"
                                           href={e.node.test1Url.value}>{e.node.test1Url.value}</a></TableRowColumn>
                        <TableRowColumn><a target="_blank"
                                           href={e.node.test2Url.value}>{e.node.test2Url.value}</a></TableRowColumn>
                        <TableRowColumn>{e.node.proportion * 100} %</TableRowColumn>
                    </TableRow>)
                })}

            </TableBody>
        </Table>
    )
};

export const abTestList = {
    query: gql`
                query abTestList{
                  allAbTests{
                    edges{
                      node{
                        id,
                        name,
                        goalUrl {value},
                        test1Url {value},
                        test2Url {value},
                        proportion,
                        shortUrl {value}
                      }
                    }
                  }
                }`,
    forceFetch: false,
    returnPartialData: true,
};

function mapQueriesToProps({ownProps, state}) {
    return {
        abTestList: abTestList,
    };
}

let selectedId = null;

@connect({mapQueriesToProps,})
export class ABTestList extends React.Component<any, any> {
    state = {
        selectedRows:[]
    };
    handleCreate = (event)=> {
        this.props.dispatch(push("/app/abtests/create"))
    };
    handleEdit = (id)=> {
        return (event)=>{
            if(!id){return}
            this.props.dispatch(push("/app/abtests/edit/" + id))
        }
    };
    handleSelection = (value)=>{
        this.setState({selectedRows:value});
    };
    render() {
        let edges:{node:ABTest}[] = [];
        if (!!this.props.abTestList) {
            if (!!this.props.abTestList.allAbTests) {
                edges = this.props.abTestList.allAbTests.edges;
            }
        }
        if(edges.length>0){
            if(this.state.selectedRows.length>0){
                selectedId = edges[this.state.selectedRows[0]].node.id
            }
        }
        return (
            <article className={styles.container}>
                <Toolbar>
                    <ToolbarTitle text="A/B Test  List"/>
                    <ToolbarGroup>
                        <ToolbarTitle text="Options"/>
                        <FontIcon className="muidocs-icon-custom-sort"/>
                        <ToolbarSeparator />
                        <RaisedButton label="Create" primary={true} onClick={this.handleCreate}/>
                        <RaisedButton label="Edit" secondary={true} onClick={this.handleEdit(selectedId)}/>

                    </ToolbarGroup>
                </Toolbar>
                <ABTable abTestList={edges} onSelection={this.handleSelection} selectedRows={this.state.selectedRows}/>
            </article>
        )

    }
}