/// <reference path='../../all.d.ts' />

import * as React from "react";
import {connect} from "react-apollo";
import gql from "graphql-tag";
const styles = require("./stats.styl");


interface ABURL {
    value:string
}
interface ABTest {
    id:string
    name:string
    goalUrl:ABURL
    test1Url:ABURL
    test2Url:ABURL
    shortUrl:ABURL
    proportion:number
}


let dab_query = gql`query deepAB($id: ID) {
    ABTest(id: $id) {
        name
        test1Url {
            value
            id
            hits(abTest: $id) {
                edges {
                    node {
                        id
                        abTest {
                            id
                        }
                        date
                    }
                }
            }
        }
        goalUrl {
            value
            id
            hits(abTest: $id) {
                edges {
                    node {
                        id
                        abTest {
                            id
                        }
                        date
                        fromUrl {
                            value,
                            id
                        }
                    }
                }
            }
        }
        test2Url {
            value
            id
            hits(abTest: $id) {
                edges {
                    node {
                        id
                        abTest {
                            id
                        }
                        date
                    }
                }

            }
        }
    }
}`;

function mapQueriesToProps({ownProps, state}) {
    return {
        deepAB: {
            query: dab_query,
            forceFetch: true,
            returnPartialData: true,
            variables: {
                id: ownProps.params.id
            }
        },
    };
}

let selectedId = null;

const UrlStats = (props)=> {
    return (
        <div className={styles.card}>
            <div className={styles.title}><b>{props.name}</b></div>
            <div><span>Url</span><span className={styles.value}>{props.url}</span></div>
            <div><span>Number of Hist</span><span className={styles.value}>{props.hits}</span></div>
            <div><span>Proportion</span><span className={styles.value}>{props.proportion}%</span></div>
            <div><span>Convertion</span><span className={styles.value}>{props.convertion}</span></div>
        </div>

    )
};

@connect({mapQueriesToProps,})
export class Stats extends React.Component<any, any> {
    render() {
        let test1 = {url:"", hits:0, proportion:0, convertion:0, name:"Test 1"};
        let test2 = {url:"", hits:0, proportion:0, convertion:0, name:"Test 2"};
        if(this.props.deepAB.ABTest){
            test1.url = this.props.deepAB.ABTest.test1Url.value;
            test2.url = this.props.deepAB.ABTest.test2Url.value;
            test1.hits = this.props.deepAB.ABTest.test1Url.hits.edges.length;
            test2.hits = this.props.deepAB.ABTest.test2Url.hits.edges.length;
            let total = test1.hits + test2.hits;
            test1.proportion =  Math.round(test1.hits / total * 100);
            test2.proportion =  Math.round(test2.hits / total * 100);
        }
        return (
            <article className={styles.container}>
                <UrlStats {...test1}/>
                <UrlStats {...test2}/>
            </article>
        )
    }
}