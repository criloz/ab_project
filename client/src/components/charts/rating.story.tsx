/// <reference path='../../all.d.ts' />

import * as React from "react";
import {storiesOf} from "@kadira/storybook";
import {RatingWidget} from "./rating";
import {Cases} from "./rating.cases";

const story = storiesOf('Rating Widget', module);

let key:string;

for(key in Cases){

    function element(iKey){
        return function(){
            return <RatingWidget {...Cases[iKey]}/>
        }
    }
    story.add(key, element(key))
}
