import * as React from "react";
const classNames = require('classnames');
const style = require("./rating.styl");

export interface RatingWidgetInterface{
    rating:number //from 0 to 100
}

export const RatingWidget = (props:RatingWidgetInterface)=> {
    let uiRating = 0;
    if(props.rating>0)uiRating=props.rating;
    if(props.rating>100)uiRating=100;
    let fillColor = "#ff5722";
    if(uiRating>50) fillColor = "#ffc107";
    if(uiRating>80) fillColor = "#8bc34a";
    uiRating  = uiRating / 100 * 220;


    return (
        <div className={style.container}>
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 5 220 40">
                <defs>
                    <clipPath id="clipping">
                        <circle cx="30" cy="25" r="15"/>
                        <circle cx="70" cy="25" r="15"/>
                        <circle cx="110" cy="25" r="15"/>
                        <circle cx="150" cy="25" r="15"/>
                        <circle cx="190" cy="25" r="15"/>
                    </clipPath>
                </defs>
                <circle cx="30" cy="25" r="15"/>
                <circle cx="70" cy="25" r="15"/>
                <circle cx="110" cy="25" r="15"/>
                <circle cx="150" cy="25" r="15"/>
                <circle cx="190" cy="25" r="15"/>
                <rect x="0" y="0" width={uiRating} height="50" clipPath="url(#clipping)" fill={fillColor}/>
            </svg>
        </div>
    )
};

