/**
 * Created by cristian on 5/06/16.
 */
import {RatingWidgetInterface} from "./rating";

export let Cases:{[id:string]:RatingWidgetInterface} = {};

Cases["Rating < 0"] = {
    rating: -452
};
Cases["Rating: 0"] = {
    rating: 0
};

Cases["Rating: < 50"] = {
    rating: 35
};

Cases["Rating: == 50"] = {
    rating: 50
};

Cases["Rating: < 80"] = {
    rating: 70
};

Cases["Rating: == 80"] = {
    rating: 80
};

Cases["Rating: > 80"] = {
    rating: 95
};

Cases["Rating: > 100"] = {
    rating: 180
};