/// <reference path='../../all.d.ts' />

import * as React from "react";
import {storiesOf} from "@kadira/storybook";
import {AccountTable, AccountTableInterface} from "./accounts_table";

const story = storiesOf('Account Table', module);

let key:string;
let Cases:{[id:string]:AccountTableInterface} = {};

Cases["basic"] = {
    accounts:[{creditor_name: "ALLY FINCL", type: "auto", id: 1, is_open: false},
              {creditor_name: "AMEX", type: "credit", id: 2, is_open: true}]
};

for(key in Cases){

    function element(iKey){
        return function(){
            return <AccountTable {...Cases[iKey]}/>
        }
    }
    story.add(key, element(key))
}
