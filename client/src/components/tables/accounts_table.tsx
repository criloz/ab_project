import * as React from "react"
import {RatingWidget} from "../charts/rating"

const style = require("./accounts_table.styl");
const classNames = require('classnames');
interface AccountInterface{
    id: number
    creditor_name: string
    type: string
    is_open: boolean
}export interface AccountTableInterface {
    accounts: AccountInterface[]
}
export class AccountTable extends React.Component<AccountTableInterface, any>{

    row(account:AccountInterface){

        let isOpenClass = {}
        isOpenClass[style.account_open] = account.is_open;
        let stateStyle = classNames(style.account_state, isOpenClass);

        return(

            <div className={style.row} key={account.id}>
                <div className={style.left_column}>
                    <i className={style[`${account.type}_icon`]}><span>30</span></i>
                </div>
                <div className={style.middle_column}>
                    <h4>{account.creditor_name}</h4>
                    <ul>
                        <li>Balance: <strong>$158,000</strong></li>
                        <li>Monthly Payment: <strong>$868</strong></li>
                        <li>Last Reported: <strong>07/07/2014</strong></li>
                    </ul>

                </div>

                <div className={style.right_column}>
                    <ul>
                        <li className={stateStyle}><i/>{account.is_open?"Open":"Closed"}</li>
                        <li style={{"margin-top":"50px"}}><RatingWidget rating={65}/></li>
                    </ul>

                </div>
            </div>


        )
    }

    render(){
        return(
        <div>
            {this.props.accounts.map(account=>this.row(account))}
        </div>
        )

    }
}