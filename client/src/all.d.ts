/*
    External libraries
 */

/// <reference path='types/react/react-global.d.ts' />
/// <reference path='types/react/react-global.d.ts' />
/// <reference path='types/redux/redux.d.ts' />
/// <reference path='types/react-redux/react-redux.d.ts' />
/// <reference path='types/react-router-redux/react-router-redux.d.ts' />
/// <reference path='types/redux-devtools/redux-devtools.d.ts' />
/// <reference path='types/redux-thunk/redux-thunk.d.ts' />
/// <reference path='types/classnames/classnames.d.ts' />
/// <reference path='types/react-motion/react-motion.d.ts' />
/// <reference path='types/chartjs/chart.d.ts' />
/// <reference path='types/material-ui/material-ui.d.ts' />
/// <reference path='types/graphql/index.d.ts' />

/// <reference path='types/isomorphic-fetch/isomorphic-fetch.d.ts' />

/// <reference path='types/Immutable.d.ts' />
/// <reference path='types/relay.d.ts' />
/// <reference path='types/react-router/react-router.d.ts' />
/// <reference path='types/jasmine/jasmine.d.ts' />
/// <reference path='types/enzyme/enzyme.d.ts' />
/// <reference path='types/js-cookie/js-cookie.d.ts' />
/// <reference path='types/moment/moment.d.ts' />



/*
    Components
 */




/*
    Reducers
 */

/// <reference path='models[redux|apollo]/app.actions.ts' />




declare function applyMixins(derivedCtor: any, baseCtors: any[]):void;

interface Window {
    React: any;
    devToolsExtension: any;
    STORE:any;
}
declare  module module{
    export var hot:any;
}

declare var require: any;

declare module "react-hot-loader" {

    export var AppContainer: __React.StatelessComponent<any>;
}

declare module "redux-multi" {
    var multi:any;
    export default multi;
}

declare module "redux-batched-subscribe"{
    export const batchedSubscribe:any;
}