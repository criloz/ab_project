// Type definitions for Redux v3.3.1
// Project: https://github.com/rackt/redux
// Definitions by: William Buchwalter <https://github.com/wbuchwalter/>, Vincent Prouillet <https://github.com/Keats/>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

declare namespace Redux {

    interface ActionCreator extends Function {
        (...args: any[]): any;
    }

    interface Reducer  {
        (state: any, action: any): any;
    }

    interface Dispatch extends Function {
        (action: any): any;
    }

    interface StoreMethods {
        dispatch: Dispatch;
        getState(): any;
    }


    interface MiddlewareArg {
        dispatch: Dispatch;
        getState: Function;
    }

    interface Middleware extends Function {
        (obj: MiddlewareArg): Function;
    }

    class Store<T> {
        getReducer(): Reducer;
        replaceReducer(nextReducer: Reducer): void;
        dispatch(action: any): any;
        getState(): T;
        subscribe(listener: Function): Function;
    }

    function createStore<T>(reducer: Reducer, initialState?: T, enhancer?: Function): Store<T>;
    function bindActionCreators<T>(actionCreators: T, dispatch: Dispatch): T;
    function combineReducers(reducers: any): Reducer;
    function applyMiddleware(...middlewares: Middleware[]): Function;
    function compose<T extends Function>(...functions: Function[]): T;
}

declare module "redux" {
    export = Redux;
}
