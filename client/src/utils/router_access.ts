import store from "../models[redux|apollo]/store";
import * as CONSTANTS from "../constants"

interface RouterReplaceFunction {
    (opts?:{ pathname:string, state?:{ nextPathname:string } }):void
};


/**
 * require backend on-line function decorator
 */

function requireBackendOnLine(target:RouterAccess, propertyKey:string, descriptor:PropertyDescriptor) {
    const oldFunction = descriptor.value
    descriptor.value = function (nextState, replace:RouterReplaceFunction) {
        const isOnLine = store.getState().app.get("backend").get("isOnLine")
        if (!isOnLine) {
            return replace({
                pathname: '/connecting',
                state: {nextPathname: nextState.location.pathname}
            })
        }
        else {
            return oldFunction(nextState, replace);
        }
    }
}

function requireAuth(target:RouterAccess, propertyKey:string, descriptor:PropertyDescriptor) {
    const oldFunction = descriptor.value;
    descriptor.value = function (nextState, replace:RouterReplaceFunction) {
        const isLogged = store.getState().app.get("session").get("isLogged");
        if (!isLogged) {
            return replace({
                pathname: '/login',
                state: {nextPathname: nextState.location.pathname}
            })
        }
        else {
            return oldFunction(nextState, replace);
        }
    }
}

/**
 *  This class control what views or routers can  be accessed  by  the user
 */
export class RouterAccess {
    /**
     * detect if an user is logged, in other case redirect to /login
     */
    @requireBackendOnLine
    @requireAuth
    static checkLogin(nextState, replace:RouterReplaceFunction) {
        replace(); // do nothing

    }

    /**
     *  2. only check the backend is on-line in other case redirect to /connecting
     */
    @requireBackendOnLine
    static checkBackendOnLine(nextState, replace:RouterReplaceFunction) {
        replace(); // do nothing

    }

    /**
     * if the user is logged, redirect to the default view if him try to access to the /login view
     */

    @requireBackendOnLine
    static checkUserNotLogged(nextState, replace:RouterReplaceFunction){
        const userIsLogged = store.getState().app.get("session").get("isLogged");
        if(userIsLogged){
            return replace({pathname: CONSTANTS.DEFAULT_ROUTER_REDIRECT})
        }
        return replace()

    }

}