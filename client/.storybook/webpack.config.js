const webpack = require('webpack');
const fs = require('fs');
const autoprefixer_stylus = require('autoprefixer-stylus');
const sgrid_stylus = require('s-grid');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

const path = require('path'),
    join = path.join,
    resolve = path.resolve;


const root = resolve(__dirname);
const tsConfigFile = join(root, 'tsconfig.json');



module.exports = {

    // Enable sourcemaps for debugging webpack's output.
    devtool: "inline-source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        loaders: [
            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.

            {
                test: /\.tsx?$/,
                loaders: ["source-map-loader", "babel?cacheDirectory", "ts-loader?configFileName=" + tsConfigFile]
            },
            {test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=application/font-woff&name=/fonts/[hash].[ext]"},
            {test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=application/font-woff&name=/fonts/[hash].[ext]"},
            {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=application/octet-stream&name=/fonts/[hash].[ext]"},
            {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?name=/fonts/[hash].[ext]"},
            {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?name=/fonts/[hash].[ext]"},
            {test: /\.(jpe?g|png)$/, loader: "file-loader?name=/img/[hash].[ext]"},

            {
                test: /\.styl$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!stylus-loader')
            },

        ]
    },
    plugins: [
        new ExtractTextPlugin('style.css', { allChunks: true })
    ],
    stylus: {
        use: [autoprefixer_stylus(), sgrid_stylus()]
    }
};
