import { configure } from '@kadira/storybook';
function loadStories() {
    // require as many stories as you need.
    require('../src/client.story');
require('../src/components/auth/login_form.story');
require('../src/components/charts/rating.story');
require('../src/components/charts/scoremeter.story');
require('../src/components/layouts/score_board.story');
require('../src/components/navigation/load_bar.story');
require('../src/components/tables/account_table.story');
require('./product_view.story');

}
configure(loadStories, module);