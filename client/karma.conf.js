// karma.conf.js
const webpack_test = require('./.storybook/webpack.config');
const webpack = require('webpack');

webpack_test["externals"]={
    'jsdom': 'window',
    //'cheerio': 'window',
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': 'window'
};

webpack_test["module"]["loaders"].push({
    test: /\.json$/,
    loader: 'json'
});

webpack_test["plugins"] = [
    // other plugins that you need
    new webpack.NormalModuleReplacementPlugin(/^\.\/package$/, function(result) {
        if(/cheerio/.test(result.context)) {
            result.request = "./package.json"
        }
    }),
    new webpack.IgnorePlugin(/ReactContext|react\/addons/)
];
module.exports = function(config) {
    config.set({
        frameworks: ['jasmine'],
        files: [
            'src/**/*.spec.tsx'
        ],
        preprocessors: {
            // add webpack as preprocessor
            'src/**/*.spec.tsx': ['webpack']
        },

        webpack: webpack_test,
    /*{
            // karma watches the test entry points
            // (you don't need to specify the entry option)
            // webpack watches dependencies

            // webpack configuration
        }*/

        webpackMiddleware: {
            // webpack-dev-middleware configuration
            // i. e.
            noInfo: true
        }
    })
};