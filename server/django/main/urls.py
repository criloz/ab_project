"""creditsecret_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views.decorators.csrf import requires_csrf_token

from graphene.contrib.django.views import GraphQLView
from main.schema import schema
from django.views.decorators.csrf import csrf_exempt

from .views import ConnectionState, login
from .views.ab import ab_view
import re

base64_pattern = r'(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$'

from django.views.generic import TemplateView


class App(TemplateView):
    template_name = "index.html"


urlpatterns = [
              url(r'^admin/', admin.site.urls),
              # login and logout views
              url(r'^connection_state/$', requires_csrf_token(ConnectionState.as_view())),
              url(r'^login/$', login),
              url(r'^ab/(?P<ab_id>{})'.format(base64_pattern), ab_view),
              url(r'^app*', App.as_view()),
              url(r'^logout/$', auth_views.logout),
              url(r'^graphql', csrf_exempt(GraphQLView.as_view(schema=schema))),
              url(r'^graphiql', include('django_graphiql.urls')),
              ]
