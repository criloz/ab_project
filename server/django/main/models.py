from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, _, \
    UserManager, timezone, \
    send_mail
from django.db import models
from graphql_relay.node.node import to_global_id


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username, password and email are required. Other fields are optional.
    """

    first_name = models.CharField(_('first name'), max_length=30)
    middle_name = models.CharField(_('middle name(s)'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30)
    username = models.CharField(max_length=30, editable=False, default='')
    email = models.EmailField(
        _('email address'),
        unique=True,
        error_messages={
            'unique': _("A user with that email already exists."),
        })

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'))
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))

    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'username']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    @property
    def full_name(self):
        return self.get_full_name()

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        " Returns the short name for the user."
        return self.first_name

    def email_user(self,
                   subject,
                   message,
                   from_email=settings.DEFAULT_FROM_EMAIL,
                   **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


class URL(models.Model):
    value = models.URLField(unique=True)


class Hit(models.Model):
    date = models.DateTimeField(auto_now=True)
    hitUrl = models.ForeignKey(URL, related_name="hits")
    ip = models.GenericIPAddressField()
    fromUrl = models.ForeignKey(URL, related_name="conversion_hits", null=True)
    abTest = models.ForeignKey('ABTest',  null=True, related_name="hits")


class ABTest(models.Model):
    name = models.CharField(max_length=255, unique=True)
    goalUrl = models.ForeignKey(URL, related_name="ab_tests")
    test1Url = models.ForeignKey(URL, related_name="tests_1")
    test2Url = models.ForeignKey(URL, related_name="tests_2")
    proportion = models.FloatField()
    shortUrl = models.ForeignKey(URL, related_name="ab_test_short", null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if self.pk is None:
            super(ABTest, self).save(force_insert, force_update, using, update_fields)

        if self.shortUrl is None:
            gid = to_global_id('ABTest', str(self.pk))
            new_url = URL(value=settings.SITE_URL + "/ab/" + gid)
            new_url.save()
            self.shortUrl = new_url

        super(ABTest, self).save(force_insert, force_update, using, update_fields)
