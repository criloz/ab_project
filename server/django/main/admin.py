from django.contrib import admin
from .models import *


# Group User  models from different app/object (auth) into one Admin block
# http://stackoverflow.com/questions/10561091/group-models-from-different-app-object-into-one-admin-block
class ProxyUser(User):
    class Meta:
        proxy = True
        app_label = 'auth'

        # set following lines to display ProxyUser as User
        verbose_name = User._meta.verbose_name
        verbose_name_plural = User._meta.verbose_name_plural


class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'first_name', 'last_name', 'is_staff', 'is_active', 'last_login')
    exclude = ('password', )
    pass


admin.site.register(ProxyUser, UserAdmin)
