from graphql_relay.node.node import from_global_id
from ..models import ABTest, Hit
from django.shortcuts import get_object_or_404, redirect
import numpy as np


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def ab_view(request, *args, **kwargs):
    _, ab_id = from_global_id(kwargs.get("ab_id"))
    ab = get_object_or_404(ABTest, pk=ab_id)
    selection = [ab.test1Url, ab.test2Url]
    p = [ab.proportion, 1-ab.proportion]
    result = np.random.choice(selection, p=p)
    new_hit = Hit(hitUrl=result, ip=(get_client_ip(request)), abTest=ab)
    new_hit.save()
    return redirect(result.value)

