# Avoid shadowing the login() and logout() views below.
from django.contrib.auth import (
    login as auth_login,
)
from django.contrib.auth.forms import (
    AuthenticationForm, )
from django.http import HttpResponse, Http404
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters


@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, authentication_form=AuthenticationForm, extra_context=None):
    """
    Displays the login form and handles the login action.
    """

    if request.method == "POST":
        form = authentication_form(request, data=request.POST)
        if form.is_valid():
            # Okay, security check complete. Log the user in.
            auth_login(request, form.get_user())
            return HttpResponse('ok')
        else:
            return HttpResponse("bad credentials", status=400)
    else:
        return HttpResponse("forbidden", status=403)
