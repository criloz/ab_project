import json

from django.http import HttpResponse
from django.middleware import csrf
from django.views.generic.base import View


class ConnectionState(View):
    def get(self, request, *args, **kwargs):
        token = csrf.get_token(request)
        return HttpResponse(
            json.dumps({"csrfToken": token, "isLogged": self.request.user.is_authenticated()}),
            content_type="application/json")
