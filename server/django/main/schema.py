import graphene
from django.core.validators import URLValidator
from django.db.utils import IntegrityError
from graphene import relay
from graphene.contrib.django import DjangoNode
from graphene.contrib.django.filter.fields import DjangoFilterConnectionField
from graphene.core.types.custom_scalars import JSONString
from .models import ABTest, URL, Hit
from graphql_relay.node.node import from_global_id

schema = graphene.Schema(name='AB Test Django Schema')

class SimpleNameConnection(relay.Connection):
    class Meta:
        type_name = 'Connection'

@schema.register
class ABTestNode(DjangoNode):
    class Meta:
        model = ABTest

@schema.register
class HitNode(DjangoNode):
    class Meta:
        model = Hit

@schema.register
class UrlNode(DjangoNode):
    hits = DjangoFilterConnectionField(HitNode)
    class Meta:
        model = URL

class Query(graphene.ObjectType):
    ABTest = relay.NodeField(ABTestNode)
    all_ab_tests = DjangoFilterConnectionField(ABTestNode,
                                               description='All the ab_tests.',
                                               connection_type=SimpleNameConnection)
    all_urls = DjangoFilterConnectionField(UrlNode,
                                           description='All the urls.',
                                           connection_type=SimpleNameConnection)



class CreateABTest(graphene.Mutation):
    class Input:
        pk = graphene.ID()
        name = graphene.String()
        goalUrl = graphene.String()
        test1Url = graphene.String()
        test2Url = graphene.String()
        proportion = graphene.Float()

    is_url = URLValidator()
    ok = graphene.Boolean()
    errors = JSONString()
    ab_test = graphene.Field('ABTestNode')

    @classmethod
    def mutate(cls, instance, args, info):
        name = args.get('name')
        goalUrl = args.get('goalUrl')
        test1Url = args.get('test1Url')
        test2Url = args.get('test2Url')
        proportion = args.get('proportion')
        pk = args.get('pk')
        ok = False
        ab_test = None
        errors = {}
        if not name.strip():
            errors["name"] = "This field is required"
        try:
            cls.is_url(goalUrl)
        except Exception as e:
            errors["goalUrl"] = str(e.message)

        try:
            cls.is_url(test1Url)
        except Exception as e:
            errors["test1Url"] = str(e.message)

        try:
            cls.is_url(test2Url)
        except Exception as e:
            errors["test2Url"] = str(e.message)

        if proportion < 0 or proportion > 1:
            errors["proportion"] = "Bad input"

        if not errors:
            goalUrl, _ = URL.objects.get_or_create(value=goalUrl)
            test1Url, _ = URL.objects.get_or_create(value=test1Url)
            test2Url, _ = URL.objects.get_or_create(value=test2Url)
            if pk:
                _, _id = from_global_id(pk)
                _id = int(_id)
                obj = ABTest.objects.get(pk=_id)
                obj.name = name
                obj.goalUrl = goalUrl
                obj.test1Url = test1Url
                obj.proportion = proportion
                obj.test2Url = test2Url
            else:
                obj = ABTest(name=name, goalUrl=goalUrl, test1Url=test1Url,
                             test2Url=test2Url, proportion=proportion)
            try:
                obj.save()
                ok = True
                ab_test = obj
            except IntegrityError:
                errors["name"] = "A/B test name should be unique"

        return CreateABTest(ab_test=ab_test, ok=ok, errors=errors)


class Mutation(graphene.ObjectType):
    create_ab_test = graphene.Field(CreateABTest)
    pass


schema.mutation = Mutation
schema.query = Query
